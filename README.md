# Note on usage

## Create definition files

The file mpim_local_tables contains the definitions so far used at MPIM. The table contains:

row 1: CF convention based long name
row 2: CF convention following unit
row 3: CF convention/CMIP defined short names, if available. Otherwise the model variable name is used
row 4: WMO grib2 triplet (discipline, category, parameter)
row 5: level type
row 6: perhaps the level height associated with the used level type

Than one can run the script parse-mpim-local-grib2.py and gets a local
directory definitions.mpim.

Further qubicc.partab can be used together with cdo to do conversions
between netcdf CF-convention and grib2.


## Changes required for using MPIM definitions

As eccodes always preferes ECMWF local definitions over the center's
encoded in the grib2 records this can lead to surprising results in
wrong names. Therefore a flag has been added to eccodes. In <path of
ecmwf/definitions>/boot.def a change is required (ecmf is the WMO
short name for ECMWF):

# ECC-806: Local concepts precedence order
transient preferLocalConcepts = 0 : hidden;

needs to be changed to

# ECC-806: Local concepts precedence order
transient preferLocalConcepts = 1 : hidden;

Additionally, parameters.def needs to replace the parameters.def in
the default eccodes definitions location for grib2.

After that ECCODES_DEFINITION_PATH has to be set:

export ECCODES_DEFINITION_PATH=<path of mpim definitions>:<path of edzw definitions>:<path of ecmf definitions>

with

mpim - MPI for Meteorology (WMO 252)
edzw - Deutscher Wetterdienst (WMO 78)
ecmf - European Centre for Medium-Range Weather Forecasts (WMO 98)

This will result in resolving the MPIM local extensions to be accepted
as first choice.

## Note: the following contains some notes for future use

mysql --host=grib-param-db-test.ecmwf.int --user=ecmwf_ro --password=ecmwf_ro param

| param | CREATE TABLE `param` (
  `id` int(11) NOT NULL,
  `shortName` varchar(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `units_id` int(11) NOT NULL,
  `description` longtext,
  `o` int(11) NOT NULL DEFAULT '1',
  `hide_web` int(11) NOT NULL DEFAULT '0',
  `hide_def` int(11) NOT NULL DEFAULT '0',
  `force128` int(11) NOT NULL DEFAULT '0',
  `insert_date` date NOT NULL DEFAULT '0000-00-00',
  `update_date` date NOT NULL DEFAULT '0000-00-00',
  `contact` varchar(1024) DEFAULT NULL,
  `urlgroup_id` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `units_id` (`units_id`),
  INDEX `shortName` (`shortName`),
  INDEX `name` (`name`),
  INDEX `name_2` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 |

| centre | CREATE TABLE `centre` (
  `id` int(11) NOT NULL,
  `abbreviation` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 |

| attribute | CREATE TABLE `attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `o` int(11) NOT NULL DEFAULT '1',
  `format` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=64 DEFAULT CHARSET=latin1 |

| units | CREATE TABLE `units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=221 DEFAULT CHARSET=latin1 |

| grib  | CREATE TABLE `grib` (
  `param_id` int(11) NOT NULL,
  `edition` int(11) NOT NULL DEFAULT '0',
  `centre` int(11) NOT NULL DEFAULT '0',
  `attribute_id` int(11) NOT NULL DEFAULT '0',
  `attribute_value` int(11) DEFAULT NULL,
  `param_version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`param_id`,`edition`,`centre`,`attribute_id`,`param_version`) USING BTREE,
  KEY `attribute_id` (`attribute_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 |

| grib2 | CREATE TABLE `grib2` (
  `disc` int(11) NOT NULL,
  `cat` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `same_units` tinyint(1) NOT NULL DEFAULT '1',
  `deprecated` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`disc`,`cat`,`number`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 |

| grib_parameters | CREATE TABLE `grib_parameters` (
  `grib_originating_centre` int(11) NOT NULL,
  `grib_code_table` int(11) NOT NULL,
  `grib_parameter` int(11) NOT NULL,
  `mars_abbreviation` text,
  `long_name` text,
  `description` text,
  `web_title` text,
  `unit` text,
  `comment` text,
  `parameter_type` int(11) DEFAULT '0',
  `wind_corresponding_parameter` int(11) DEFAULT '0',
  `netcdf_name` text,
  `netcdf_cf_approved` int(11) DEFAULT '0',
  `magics_abbreviated_text` text,
  `magics_title` text,
  `magics_offset` double DEFAULT NULL,
  `magics_factor` double DEFAULT NULL,
  `magics_scaled_unit` text,
  `magics_contour_interval` double DEFAULT NULL,
  `magics_specification_group` text,
  `magics_comment` text,
  `dissemination_accuracy` int(11) DEFAULT '0',
  `dissemination` int(11) DEFAULT '0',
  `insert_date` date DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  PRIMARY KEY (`grib_originating_centre`,`grib_code_table`,`grib_parameter`),
  INDEX `grib_code_table` (`grib_code_table`),
  INDEX `parameter_type` (`parameter_type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 |

name.def:
'Sea ice area fraction' = {
	 discipline = 10 ;
	 parameterCategory = 2 ;
	 parameterNumber = 0 ;
	}
        
shortName.def:
#Sea ice area fraction
'ci' = {
	 discipline = 10 ;
	 parameterCategory = 2 ;
	 parameterNumber = 0 ;
	}

units.def:
#Sea ice area fraction
'(0 - 1)' = {
	 discipline = 10 ;
	 parameterCategory = 2 ;
	 parameterNumber = 0 ;
	}

#Sea ice area fraction
'31' = {
	 discipline = 10 ;
	 parameterCategory = 2 ;
	 parameterNumber = 0 ;
	}


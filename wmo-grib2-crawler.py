#! /usr/bin/env python3

import csv
import urllib.request
import re

url = "https://raw.githubusercontent.com/wmo-im/GRIB2/master/GRIB2_CodeFlag_4_2_CodeTable_en.csv"

response = urllib.request.urlopen(url)
lines = [l.decode('utf-8') for l in response.readlines()]
cr = csv.reader(lines)

for row in cr:
    (title, subtitle, codeflag, value, description, note, unit, status) = tuple(row)
    if "Title_en" in title:
        continue
    if "Reserved" not in description and "Missing" not in description:
        (discipline, category, *dummy) = tuple(re.findall(r'\d+', subtitle))
        print(discipline, " ", category, " ", codeflag, " - ", description, " - ", unit)




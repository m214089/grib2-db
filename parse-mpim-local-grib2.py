#! /usr/bin/env python3
#_______________________________________________________________________________
#
from operator import attrgetter

class Partab:
    def __init__(self, paramId, shortName, name, unit, discipline, category, parameter, levelType = None, height = None):
        self.paramId    = paramId
        self.shortName  = shortName.strip()
        self.name       = name.strip()
        self.unit       = unit.strip()
        self.discipline = int(discipline)
        self.category   = int(category)
        self.parameter  = int(parameter)
        self.levelType  = levelType
        self.height     = height
        
    def __repr__(self):
        return repr((self.paramId, self.shortName, self.name, self.discipline, self.category, self.parameter))

class Level:
    def __init__(self, levelType, factor = None, value = None):
        self.typeOfFirstFixedSurface = levelType
        self.scaleFactorOfFirstFixedSurface = factor
        self.scaledValueOfFirstFixedSurface = value

    def __repr__(self):
        if self.scaleFactorOfFirstFixedSurface is None and self.scaledValueOfFirstFixedSurface is None:
            text = "typeOfFirstFixedSurface = {} ;".format(self.typeOfFirstFixedSurface)
        else:
            text = """typeOfFirstFixedSurface = {0} ;
    scaleFactorOfFirstFixedSurface = {1} ;
    scaledValueOfFirstFixedSurface = {2} ; """.format(self.typeOfFirstFixedSurface,
                                                      self.scaleFactorOfFirstFixedSurface,
                                                      self.scaledValueOfFirstFixedSurface)
        return(repr(text))

    def __str__(self):
        if self.scaleFactorOfFirstFixedSurface is None and self.scaledValueOfFirstFixedSurface is None:
            text = "typeOfFirstFixedSurface = {} ; ".format(self.typeOfFirstFixedSurface)
        else:
            text = """typeOfFirstFixedSurface = {0} ;
    scaleFactorOfFirstFixedSurface = {1} ;
    scaledValueOfFirstFixedSurface = {2} ; """.format(self.typeOfFirstFixedSurface,
                                                      self.scaleFactorOfFirstFixedSurface,
                                                      self.scaledValueOfFirstFixedSurface)
        return(text)
    
sfc = Level(1)        
tropo = Level(7)
toa = Level(8)
vint = Level(10)
msl = Level(101)
h2m = Level(103, 0, 2)
h10m = Level(103, 0, 10)

# print(sfc)
# print(tropo)
# print(toa)
# print(vint)
# print(msl)
# print(h2m)
# print(h10m)

#_______________________________________________________________________________
#
# file: grib2/tables/local/mpim/1/4.2.<discipline>.<category>.table
#       requires only the local entries - all others are collected
#       from top level WMO defined numbers.
#_______________________________________________________________________________
#
file_partab = open("qubicc.partab", "w")

file_paramid_master   = open("definitions.mpim/grib2/paramId.def", "w")
file_shortname_master =  open("definitions.mpim/grib2/shortName.def", "w")
file_name_master      =  open("definitions.mpim/grib2/name.def", "w")
file_units_master     =  open("definitions.mpim/grib2/units.def", "w")

file_paramid_local   = open("definitions.mpim/grib2/localConcepts/mpim/paramId.def", "w")
file_shortname_local =  open("definitions.mpim/grib2/localConcepts/mpim/shortName.def", "w")
file_name_local       =  open("definitions.mpim/grib2/localConcepts/mpim/name.def", "w")
file_units_local     =  open("definitions.mpim/grib2/localConcepts/mpim/units.def", "w")

files_table_4_2 = {}

with open('mpim-local-tables') as f:
    lines = f.readlines()

partab_entries = []
    
paramId = 86000  # to be replacced by a database index of a variable  
for line in lines:
    paramId = paramId + 1
    try:
        _, name, unit, shortName, triple, levelType, height, _ = line.split("|")
    except:
        print(line)
    discipline, category, parameter = triple.split(",")
    if levelType.isspace(): 
        partab_entries.append(Partab(paramId, shortName, name, unit, discipline, category, parameter))    
    else:
        if height.isspace():
            partab_entries.append(Partab(paramId, shortName, name, unit, discipline, category, parameter, levelType))                
        else:
            partab_entries.append(Partab(paramId, shortName, name, unit, discipline, category, parameter, levelType, int(height)))

partab_entries.sort(key=attrgetter('discipline', 'category', 'parameter'))

for entry in partab_entries:

    paramId = entry.paramId
    name = entry.name
    unit = entry.unit
    shortName = entry.shortName
    discipline = entry.discipline
    category = entry.category    
    parameter = entry.parameter
    levelType = entry.levelType
    height = entry.height

#    print("paramId {} ".format(entry.paramId), end='')
#    print("name {} ".format(entry.name), end='')
#    print("unit {} ".format(entry.unit), end='')
#    print("shortName {} ".format(entry.shortName), end='')
#    print("{} ".format(entry.discipline), end='')
#    print("{} ".format(entry.category), end='')
#    print("{} ".format(entry.parameter), end='')

    level_text = ""
    if levelType is not None:
        if height is not None:
#            print(" height {}".format(height), end='')
            if height == 2:
                level_text = str(h2m)
            if height == 10:                
                level_text = str(h10m)
        else:
                level_text = str(vars()[levelType.strip()])
    
    #___________________________________________________________________________
    # for cdo setpartabn:
    file_partab.write("""
&parameter name='{0}' long_name='{1}' units='{2}' param={3:d}.{4:d}.{5:d} /\
          """.format(shortName, name, unit, parameter, category, discipline))

    #___________________________________________________________________________
    # for eccodes local tables:
    if discipline >= 192 or category >= 192 or parameter >= 192:
        table_filename = "definitions.mpim/grib2/tables/local/mpim/1/4.2.{0}.{1}.table".format(discipline, category)
        if table_filename not in files_table_4_2:
            files_table_4_2[table_filename] = open(table_filename, "w")
            files_table_4_2[table_filename].write("# mpim local table version\n") 
        files_table_4_2[table_filename].write("{0} {0} {1}\n".format(parameter, name))

    #___________________________________________________________________________
    # paramID.def


    paramId_text = """
#paramId: {4:d}
#{0}
'{4:d}' = {{
    discipline = {1} ;
    parameterCategory = {2} ;    
    parameterNumber = {3} ;
    {5}   
   }}
          """.format(name, discipline, category, parameter, paramId, level_text)
    
    file_paramid_master.write(paramId_text)
    file_paramid_local.write(paramId_text)
        
    #___________________________________________________________________________
    # shortName.def

    shortname_text = """
#paramId: {6:d}
#{0}
'{4}' = {{
    discipline = {1} ;
    parameterCategory = {2} ;    
    parameterNumber = {3} ;   
    {5}   
   }}
          """.format(name, discipline, category, parameter, shortName, level_text, paramId)

    file_shortname_master.write(shortname_text)
    file_shortname_local.write(shortname_text)

    #___________________________________________________________________________
    # name.def

    name_text = """
#paramId: {5:d}
#{0}
'{0}' = {{
    discipline = {1} ;
    parameterCategory = {2} ;    
    parameterNumber = {3} ;   
    {4}   
   }}
          """.format(name, discipline, category, parameter, level_text, paramId)
    file_name_master.write(name_text)
    file_name_local.write(name_text)

    #___________________________________________________________________________
    # units.def

    units_text = """
#paramId: {6:d}
#{0}
'{4}' = {{
    discipline = {1} ;
    parameterCategory = {2} ;    
    parameterNumber = {3} ;   
    {5}   
   }}
          """.format(name, discipline, category, parameter, unit, level_text, paramId)

    file_units_master.write(units_text)
    file_units_local.write(units_text)
    
    #___________________________________________________________________________
    
file_partab.close()

file_paramid_master.close()
file_shortname_master.close()
file_name_master.close()
file_units_master.close()

file_paramid_local.close()
file_shortname_local.close()
file_name_local.close()
file_units_local.close()

for filename, f in files_table_4_2.items():
    f.close()
    
with open('parameters.def', 'w') as r:
    r.write(""" 
# (C) Copyright 2005- ECMWF.  
#
# with changes for a different preferred selection of localConcepts from 
# local center instead of ECMWF concepts.

transient dummyc=0: hidden;


concept paramIdLegacyECMF(defaultParameter,"paramId.legacy.def",conceptsMasterDir,conceptsLocalDirECMF): long_type,no_copy,hidden;
concept paramIdECMF (paramIdLegacyECMF,"paramId.def",conceptsMasterDir,conceptsLocalDirECMF): long_type,no_copy;
concept paramId (paramIdECMF,"paramId.def",conceptsLocalDirAll,conceptsMasterDir): long_type;

concept shortNameLegacyECMF(defaultShortName,"shortName.legacy.def",conceptsMasterDir,conceptsLocalDirECMF): no_copy,dump,hidden;
concept shortNameECMF (shortNameLegacyECMF,"shortName.def",conceptsMasterDir,conceptsLocalDirECMF): no_copy,dump;
concept ls.shortName (shortNameECMF,"shortName.def",conceptsLocalDirAll,conceptsMasterDir): no_copy,dump;

concept unitsLegacyECMF(defaultName,"units.legacy.def",conceptsMasterDir,conceptsLocalDirECMF): no_copy,hidden;
concept unitsECMF (unitsLegacyECMF,"units.def",conceptsMasterDir,conceptsLocalDirECMF): no_copy;
concept units (unitsECMF,"units.def",conceptsLocalDirAll,conceptsMasterDir): no_copy;

concept nameLegacyECMF(defaultName,"name.legacy.def",conceptsMasterDir,conceptsLocalDirECMF): no_copy,dump,hidden;
concept nameECMF(nameLegacyECMF,"name.def",conceptsMasterDir,conceptsLocalDirECMF): no_copy,dump;
concept name(nameECMF,"name.def",conceptsLocalDirAll,conceptsMasterDir): no_copy,dump;

concept cfNameLegacyECMF(defaultShortName,"cfName.legacy.def",conceptsMasterDir,conceptsLocalDirECMF): no_copy,dump,hidden;
concept cfNameECMF(cfNameLegacyECMF,"cfName.def",conceptsMasterDir,conceptsLocalDirECMF) : no_copy,dump;
concept cfName(cfNameECMF,"cfName.def",conceptsLocalDirAll,conceptsMasterDir) : no_copy,dump;

concept cfVarNameLegacyECMF(defaultShortName,"cfVarName.legacy.def",conceptsMasterDir,conceptsLocalDirECMF): no_copy,dump,hidden;
concept cfVarNameECMF (cfVarNameLegacyECMF,"cfVarName.def",conceptsMasterDir,conceptsLocalDirECMF): no_copy,dump;
concept cfVarName (cfVarNameECMF,"cfVarName.def",conceptsLocalDirAll,conceptsMasterDir): no_copy,dump;

# modelName: Contribution from Daniel Lee @ DWD
concept modelName (defaultName,"modelName.def",conceptsLocalDirAll,conceptsMasterDir): no_copy,dump,read_only;

template_nofail names "grib2/products_[productionStatusOfProcessedData].def";

meta ifsParam ifs_param(paramId,type);
""")

#! /usr/bin/env python3
#______________________________________________________________________________
#
# use sqlite for initial development
#
# some helper functions

def create_connection(db_file):

    conn = None
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return conn

def create_table(conn, create_table_sql):

    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)

def main():
    database = r"eccodes.db"

    sql_create_parameter_table =
        """ CREATE TABLE param (
            id                      int(11)      NOT NULL,
            shortName               varchar(20)  DEFAULT NULL,
            insert_date             date NOT NULL DEFAULT '0000-00-00',
            update_date             date NOT NULL DEFAULT '0000-00-00',
            contact                 varchar(1024) DEFAULT NULL,
            PRIMARY KEY (id, shortName, name)
            FOREIGN KEY (trackartist) REFERENCES artist(artistid)
            ); """


    sql_create_grib_parameter_table =
        """ CREATE TABLE grib_parameters (
            parameter_id            int NOT NULL
            grib_centre             int(11) NOT NULL,
            grib_subcentre          int(11) NOT NULL,
            grib_discipline         int(11) NOT NULL,
            grib_category           int(11) NOT NULL,
            grib_parameter          int(11) NOT NULL,
            long_name               text,
            description             text,
            unit                    text,
            comment                 text,
            parameter_type          int(11) DEFAULT '0',
            netcdf_name             text,
            netcdf_cf_approved      int(11) DEFAULT '0',
            insert_date             date DEFAULT NULL,
            update_date             date DEFAULT NULL,
            PRIMARY KEY (grib_centre, grib_subcenter, grib_discipline, grib_category, grib_parameter),
            ); """
    
    # create a database connection
    conn = create_connection(database)

    # create tables
    if conn is not None:
        # create projects table
        create_table(conn, sql_create_projects_table)

        # create tasks table
        create_table(conn, sql_create_tasks_table)
    else:
        print("Error! cannot create the database connection.")

#______________________________________________________________________________

if __name__ == '__main__':
    main()
#______________________________________________________________________________



